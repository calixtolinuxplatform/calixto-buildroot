#!/bin/bash

BOARD_DIR="board/calixto/stamp"
IMAGE_DIR="calixto-images/stamp/buildroot-image/"
TEMP_DIR=$IMAGE_DIR/"temp"
IMAGE_DIR1=$IMAGE_DIR"NAND-ethflasher/"
IMAGE_DIR2=$IMAGE_DIR"SD_Card/"
OUT_DIR1=$IMAGE_DIR1/
OUT_DIR2=$IMAGE_DIR2/

CREATE_IMAGE="create-flasher-image.sh"
DEBRICK_SCR="debrick.scr"
OUT_FILE="calixto-stamp.out"

mkdir -p $TEMP_DIR

cp output/images/MLO.byteswap $TEMP_DIR
cp output/images/u-boot.img $TEMP_DIR
cp output/images/am335x-calixto-stamp.dtb $TEMP_DIR
cp output/images/zImage $TEMP_DIR
cp output/images/rootfs.ubi $TEMP_DIR

cp $BOARD_DIR/$CREATE_IMAGE $TEMP_DIR

cd $TEMP_DIR

./$CREATE_IMAGE

cd -

mkdir -p $OUT_DIR1

cp $TEMP_DIR/$DEBRICK_SCR $OUT_DIR1
cp $TEMP_DIR/$OUT_FILE $OUT_DIR1

cp $TEMP_DIR/$DEBRICK_SCR $IMAGE_DIR1
cp $TEMP_DIR/$OUT_FILE $IMAGE_DIR1

sync

rm -rf $TEMP_DIR

mkdir -p $OUT_DIR2
cp output/images/MLO $OUT_DIR2
cp output/images/MLO.byteswap $OUT_DIR2
cp output/images/u-boot.img $OUT_DIR2
cp output/images/rootfs.tar $OUT_DIR2
cp $BOARD_DIR/create-sdcard-boot.sh $OUT_DIR2

IMAGE_DIR3=$IMAGE_DIR"nfs/"
OUT_DIR3=$IMAGE_DIR3/

mkdir -p $OUT_DIR3

cp output/images/zImage $OUT_DIR3
cp output/images/am335x-calixto-stamp.dtb $OUT_DIR3
cp output/images/rootfs.tar $OUT_DIR3
