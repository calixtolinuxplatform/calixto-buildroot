#!/bin/sh

UBINIZEFILE="ubinize.cfg"

mkdir -p /media/mmcblk0p2
mkdir -p rootfs

rm -rf /media/mmcblk0p2/*
rm -rf rootfs/*

if [ ! -e "$UBINIZEFILE" ] ; then
    touch "$UBINIZEFILE"
fi

cat > $UBINIZEFILE << EOF1
[ubifs]
mode=ubi
image = rootfs.ubifs
vol_id=0
vol_size=230MiB
vol_type=dynamic
vol_name=calixto-stamp-rootfs
vol_flags=autoresize
EOF1

echo "Copying files to rootfs directory.. [Ignore Any recursive error messaages]"
mount /dev/mmcblk0p2 /media/mmcblk0p2 &> /dev/null
cp -rf /media/mmcblk0p2/* rootfs/
sync
sync
sync

echo "Creating UBIFS image.."
mkfs.ubifs -r rootfs -F -o rootfs.ubifs -m 2048 -e 126976 -c 1895

echo "Creating UBI image.."
ubinize -o rootfs.ubi -m 2048 -p 128KiB -s 2048 -O 2048 ubinize.cfg

echo "Erasing and flashing dtb.."
flash_erase /dev/mtd0 0 0
nandwrite -p /dev/mtd0 /boot/am335x-calixto-stamp.dtb

echo "Erasing and flashing zImage.."
flash_erase /dev/mtd1 0 0
nandwrite -p /dev/mtd1 /boot/zImage

echo "Erasing and flashing rootfs.."
flash_erase /dev/mtd3 0 0
nandwrite -p /dev/mtd3 rootfs.ubi

sync
sync
echo "Removing unwanted files.."
umount /media/mmcblk0p2
rm -rf rootfs*
sync
sync

echo "Flashing completed.."
