#!/bin/bash
echo
echo  "Executing Prerequisite package installation script..."
echo

AMIROOT=`whoami | awk {'print $1'}`
if [ "$AMIROOT" != "root" ] ; then

	echo -e "\e[31m	**** Error *** must run script with sudo"
	echo -e "\e[39m"
	exit
fi

if lsb_release -d | grep "Ubuntu 14.04"
then
	echo
	echo
	apt-get update
	apt-get install build-essential
	apt-get install uboot-tools
	apt-get install libx32ncurses5
	apt-get install libx32ncurses5-dev

	if uname -m | grep "x86_64"
	then
  	apt-get install lib32stdc++6 lib32z1 lib32z1-dev
	fi
elif lsb_release -d | grep "Ubuntu 16.04"
then
	echo
	echo
	apt-get update
	apt-get install build-essential
	apt-get install uboot-tools
	apt-get install libx32ncurses5
	apt-get install libx32ncurses5-dev

	if uname -m | grep "x86_64"
	then
  	apt-get install lib32stdc++6 lib32z1 lib32z1-dev
	fi

elif lsb_release -d | grep "Ubuntu 12.04"
then
	echo
	echo

	if uname -m | grep "x86_64"
	then
  	dpkg --add-architecture i386
	fi

	apt-get update

	if uname -m | grep "x86_64"
	then
  	apt-get install ia32-libs
	fi

	apt-get install build-essential
	apt-get install uboot-mkimage
	apt-get install lib32ncurses5
	apt-get install lib32ncurses5-dev

else

	echo
	echo -e "\e[31m[31mWarning: Your Distribution has not been tested by Calixto..."
	echo "please install the corresponding packages for your distribution...."
	echo -e "\e[39"
	echo

	exit 

fi

echo "Executed Prerequisite installation script"
