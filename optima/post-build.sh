#!/bin/sh

BOARD_DIR="board/calixto/optima/"

install -d -m 755 $TARGET_DIR/lib/firmware/
install -D -m 755 $BOARD_DIR/amx3-cm3/am335x-pm-firmware.elf 	$TARGET_DIR/lib/firmware/

install -D -m 755 $BOARD_DIR/u-boot-fw-utils_2013.10/fw_printenv	$TARGET_DIR/sbin/
install -D -m 755 $BOARD_DIR/u-boot-fw-utils_2013.10/fw_setenv 		$TARGET_DIR/sbin/
install -D -m 644 $BOARD_DIR/u-boot-fw-utils_2013.10/fw_env.config 	$TARGET_DIR/etc/

install -D -m 755 $BOARD_DIR/init-ifupdown/interfaces 			$TARGET_DIR/etc/network/interfaces

install -D -m 755 $BOARD_DIR/filesystem/S60Alsa                         $TARGET_DIR/etc/init.d/
install -D -m 755 $BOARD_DIR/filesystem/profile                         $TARGET_DIR/etc/

install -D -m 755 $BOARD_DIR/flash_emmc.sh				$TARGET_DIR/etc/

install -d -m 755 $TARGET_DIR/usr/sounds
install -D -m 755 $BOARD_DIR/filesystem/sounds/*			$TARGET_DIR/usr/sounds/
