#!/bin/bash

BOARD_DIR="board/calixto/optima"
IMAGE_DIR="calixto-images/optima256/buildroot-image/"


IMAGE_DIR1=$IMAGE_DIR"SD_Card/"
OUT_DIR1=$IMAGE_DIR1/


mkdir -p $OUT_DIR1

cp $BOARD_DIR/create-sdcard-boot.sh $OUT_DIR1
cp output/images/MLO $OUT_DIR1
cp output/images/MLO.byteswap $OUT_DIR1
cp output/images/u-boot.img $OUT_DIR1
cp output/images/rootfs.tar $OUT_DIR1

IMAGE_DIR2=$IMAGE_DIR"eMMC-ethflasher/"
OUT_DIR2=$IMAGE_DIR2/

mkdir -p $OUT_DIR2

cp $BOARD_DIR/ethflasher/* $OUT_DIR2
cp output/images/rootfs.tar $OUT_DIR2

IMAGE_DIR3=$IMAGE_DIR"nfs/"
OUT_DIR3=$IMAGE_DIR3/

mkdir -p $OUT_DIR3

cp output/images/zImage $OUT_DIR3
cp output/images/am335x-calixto-optima.dtb $OUT_DIR3
cp output/images/rootfs.tar $OUT_DIR3






